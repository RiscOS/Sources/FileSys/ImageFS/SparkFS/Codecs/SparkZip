/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/Codecs/SparkZip/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.convert */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "kernel.h"
#include "swis.h"

#include "SparkLib/zflex.h"

#include "Interface/SparkFS.h"
#include "AsmUtils/rminfo.h"

#include "SparkLib/err.h"
#include "SparkLib/sfs.h"
#include "SparkLib/zbuffer.h"
#include "SparkLib/sarcfs.h"
#include "SparkLib/sinterface.h"
#include "SparkLib/smsdos2.h"

#include "zipdefs.h"
#include "arcs.h"
#include "unpack.h"
#include "pack.h"
#include "cat.h"
#include "convert.h"


/*****************************************************************************/



#define VERSIONTOX    20
#define OSTYPE        0xD00
#define VERSIONMADEBY (20 | OSTYPE)



/* return 1 if get extra field */

int parseextra(heads * hdr,char * p,int len)
{
 int code;
 int slen;

 while(len>4)
 {
  code=*p++;
  code+=(*p++)<<8;

  slen=*p++;
  slen+=(*p++)<<8;

  if(code==0x4341)   /* a hit */
  {
   if(!memcmp(p,"ARC0",4))
   {                                    // DP 1/11/2007
    hdr->load=getuint32((uint8*)(p+4));
    hdr->exec=getuint32((uint8*)(p+8));
    hdr->acc=getuint32((uint8*)(p+12));
    if(len==20 || len==24) return(2);   // DP 1/11/2007 it is a SparkFS header and contains nothing else
    return(1);
   }                                    // DP 1/11/2007
  }
  p+=slen;
  len-=slen+4;
 }
 return(0);
}


static int writeextra(heads * hdr,char * p)
{
 int len;

 len=4+16; /* was +4; */

 *p++=0x41;
 *p++=0x43;
 *p++=len;
 *p++=len>>8;
 *p++='A';
 *p++='R';
 *p++='C';
 *p++='0';

 putuint32((uint8*)p,hdr->load);
 p+=4;

 putuint32((uint8*)p,hdr->exec);
 p+=4;

 putuint32((uint8*)p,hdr->acc);
 p+=4;

 putuint32((uint8*)p,0);
 p+=4;

 return(len+4);  /* was len */
}



/* given the file offset, find the corresponding offset of the cd entry */

int findcd(archive * arc,int fn)
{
 central_header * cp;
 end_header     * ep;
 int                             offset;
 int                             l;
 int                             n;
 int                             i;
 int                             len;
 int                             rlen;

 offset=arc->hdr[fn].fp;
 l=0;

 ep=(end_header *)(arc->end);
  n=ep->total_entries_central_dir;

 for(i=0;i<n;i++)
 {
  cp=(central_header *)(arc->cent+l);

  len =cp->filename_length;
  len+=cp->extra_field_length;
  len+=cp->file_comment_length;
  rlen=len;
  len++;
  len=(len+3) & 0xFFFFFFFC;

  if((offset-4)==cp->relative_offset_local_header) return(l);

  l+=len+sizeof(central_header);
 }

 return(-1);
}



/* remove the central dir entry at this offset         */
/* update arc->loc, and the end of dir heaer          */
/* chunk is the amount of file removed                 */
/* target is the file offset of the file to be removed */

void cdrem(archive * arc,int off,int chunk)
{
 central_header * cp;
 end_header     * ep;
 int              len;
 int              rlen;
 int              size;


 cp=(central_header *)(arc->cent+off);

 len=sizeof(central_header);
 len+=cp->filename_length;
 len+=cp->extra_field_length;
 len+=cp->file_comment_length;
 len++;
 len=(len+3) & 0xFFFFFFFC;

 rlen=central_header_size;
 rlen+=cp->filename_length;
 rlen+=cp->extra_field_length;
 rlen+=cp->file_comment_length;
 rlen+=4;

 size=arc->size;
 arc->size-=len;


 if(size-off-len) memmove((char*)cp,((char*)cp)+len,size-off-len);
 flex_extend((flex_ptr)&(arc->cent),size-len);

 /* now changed copy of central dir in memory */

 ep=(end_header *)(arc->end);

 ep->total_entries_central_dir_on_this_disk-=1;
 ep->total_entries_central_dir-=1;
 ep->size_central_directory-=rlen;
 ep->offset_start_central_directory-=chunk;

 arc->loc-=chunk;
}




void cdshuffle(archive * arc,int chunk,int target)
{
 central_header * chdr;
 int              off;
 int              len;
 int              rlen;

 off=0;

 while(off<arc->size)
 {
  chdr=(central_header *)(arc->cent+off);

  if(chdr->relative_offset_local_header>=target) chdr->relative_offset_local_header-=chunk;

  len =chdr->filename_length;
  len+=chdr->extra_field_length;
  len+=chdr->file_comment_length;
  rlen=len;
  len++;
  len=(len+3) & 0xFFFFFFFC;

  off+=len+sizeof(central_header);
 }
}



static void initendheader(end_header * r)
{
 r->number_this_disk=0;
 r->number_disk_with_start_central_directory=0;
 r->total_entries_central_dir_on_this_disk=0;
 r->total_entries_central_dir=0;
 r->size_central_directory=0;
 r->offset_start_central_directory=0;
 r->zipfile_comment_length=0;
 r->comment[0]=0;
}



/* auto set file extent */

static _kernel_oserror * writeendheader(archive * arc,end_header * ehdr)
{
 _kernel_oserror       * err;
 uint8                   hdr[end_header_size];
 uint8                   sig8[4];
 int                     fp;

 putuint32(sig8,END_SIGNATURE);

 putuint16(hdr+e_number_this_disk_offset,ehdr->number_this_disk);
 putuint16(hdr+e_number_disk_with_start_central_directory_offset,ehdr->number_disk_with_start_central_directory);
 putuint16(hdr+e_total_entries_central_dir_on_this_disk_offset,ehdr->total_entries_central_dir_on_this_disk);
 putuint16(hdr+e_total_entries_central_dir_offset,ehdr->total_entries_central_dir);
 putuint32(hdr+e_size_central_directory_offset,ehdr->size_central_directory);
 putuint32(hdr+e_offset_start_central_directory_offset,ehdr->offset_start_central_directory);
 putuint16(hdr+e_zipfile_comment_length_offset,ehdr->zipfile_comment_length);

 err=tellarc(arc,&fp);

          err=writearc(arc,(char*)sig8,4);
 if(!err) err=writearc(arc,(char*)hdr,end_header_size);
 if(!err) err=writearc(arc,ehdr->comment,ehdr->zipfile_comment_length);

 if(!err) err=tellarc(arc,&fp);
 if(!err) err=setfileextent(arc->fh,fp);

 return(err);
}




/* auto write end of central dir */

_kernel_oserror * writecentraldirlo(archive * arc)
{
 _kernel_oserror               * err;
 uint8                           hdr[central_header_size];
 central_header * chdr;
 end_header *        ep;
 uint8                           sig8[4];
 int                             off;
 int                             len;
 int                             rlen;



 err=seekarc(arc,arc->loc-4);
 if(!err)
 {
  ep=(end_header *)(arc->end);
  off=0;

  while(off<arc->size)
  {
   chdr=(central_header *)(arc->cent+off);

   putuint32(sig8,CENTRAL_SIGNATURE);

   putuint16(hdr+c_version_made_by_offset,chdr->version_made_by);
   putuint16(hdr+c_version_needed_to_extract_offset,chdr->version_needed_to_extract);
   putuint16(hdr+c_general_purpose_bit_flag_offset,chdr->general_purpose_bit_flag);
   putuint16(hdr+c_compression_method_offset,chdr->compression_method);
   putuint16(hdr+c_last_mod_file_time_offset,chdr->last_mod_file_time);
   putuint16(hdr+c_last_mod_file_date_offset,chdr->last_mod_file_date);
   putuint32(hdr+c_crc32_offset,chdr->crc32);
   putuint32(hdr+c_compressed_size_offset,chdr->compressed_size);
   putuint32(hdr+c_uncompressed_size_offset,chdr->uncompressed_size);
   putuint16(hdr+c_filename_length_offset,chdr->filename_length);
   putuint16(hdr+c_extra_field_length_offset,chdr->extra_field_length);
   putuint16(hdr+c_file_comment_length_offset,chdr->file_comment_length);
   putuint16(hdr+c_disk_number_start_offset,chdr->disk_number_start);
   putuint16(hdr+c_internal_file_attributes_offset,chdr->internal_file_attributes);
   putuint32(hdr+c_external_file_attributes_offset,chdr->external_file_attributes);

   putuint32(hdr+c_relative_offset_local_header_offset,chdr->relative_offset_local_header);

   len =chdr->filename_length;
   len+=chdr->extra_field_length;
   len+=chdr->file_comment_length;
   rlen=len;
   len++;
   len=(len+3) & 0xFFFFFFFC;

            err=writearc(arc,(char*)sig8,4);
   if(!err) err=writearc(arc,(char*)hdr,central_header_size);

   if(!err) err=writearc(arc,chdr->extra,rlen);

   if(err) break;

   off+=len+sizeof(central_header);
  }

  if(!err) err=writeendheader(arc,ep);
 }
 return(err);
}




_kernel_oserror * writecentraldir(archive * arc)
{
 _kernel_oserror * err;

 err=NULL;

 if(arc->docache)
 {
  arc->cachestate=ZIPWRITECD;
  deb("cs=%d",arc->cachestate);
 }
 else
 {
  err=writecentraldirlo(arc);
 }

 return(err);
}





_kernel_oserror * writefilehdr(archive * arc,heads * hdr,char * name,int opt)
{
 _kernel_oserror  * err;
 uint8              filehdr[file_header_size];
 uint8              sig8[4];
 unsigned short int time;
 unsigned short int date;
 char               extra[64];
 int                exlen;
 unsigned int       size;
 unsigned int       length;


 exlen=writeextra(hdr,extra);


 if(hdr->dirn==-1) // fix 14/6/2015 problem with change of dir access bits writing non-zero lengths to file
                   // because hdr structure is filled with length of what is in the folder on load
                   // but zip code does not keep this info updated
 {
  size=hdr->size;
  length=hdr->length;
 }
 else
 {
  size=length=0;
 }


 arcgetstamp(hdr->load,hdr->exec,&date,&time);


 putuint32(sig8,FILE_SIGNATURE);

 putuint16(filehdr+version_needed_to_extract_offset,VERSIONTOX);
 putuint16(filehdr+general_purpose_bit_flag_offset,opt);
 putuint16(filehdr+compression_method_offset,hdr->hdrver);
 putuint16(filehdr+last_mod_file_time_offset,time);
 putuint16(filehdr+last_mod_file_date_offset,date);
 putuint32(filehdr+crc32_offset,hdr->crc);
 putuint32(filehdr+compressed_size_offset,size);
 putuint32(filehdr+uncompressed_size_offset,length);
 putuint16(filehdr+filename_length_offset,strlen(name));
 putuint16(filehdr+extra_field_length_offset,exlen);

          err=writearc(arc,(char*)sig8,4);
 if(!err) err=writearc(arc,(char*)filehdr,file_header_size);

 if(!err) err=writearc(arc,name,strlen(name));
 if(!err) err=writearc(arc,extra,exlen);

 hdr->hsize=file_header_size+strlen(name)+exlen;

 return(err);
}




/* insert this file in the central directory */

_kernel_oserror * cdins(archive * arc,int fn,char * name,int chunk,int opt)
{
 _kernel_oserror               * err;
 central_header  * cp;
 end_header      * ep;
 heads                         * hdr;
 int                             len;
 int                             rlen;
 int                             size;
 unsigned short              int time;
 unsigned short              int date;
 char                            extra[64];
 int                             exlen;
 int                             attr;
 int                             st_mode;
 unsigned int                    xsize;
 unsigned int                    xlength;



 exlen=writeextra(&arc->hdr[fn],extra);

 len=sizeof(central_header);
 len+=strlen(name);
 len+=exlen; /* extra_field_length */
 len+=0; /* file_comment_length */
 len++;
 len=(len+3) & 0xFFFFFFFC;


 rlen=central_header_size+4;
 rlen+=strlen(name);
 rlen+=exlen; /* extra_field_length */
 rlen+=0; /* file_comment_length */


 size=arc->size+len;
 err=flex_extend((flex_ptr)&(arc->cent),size);
 if(!err)
 {
  cp=(central_header *)(arc->cent+arc->size);
  arc->size=size;

  hdr=&arc->hdr[fn];
  arcgetstamp(hdr->load,hdr->exec,&date,&time);

  if(hdr->dirn==-1) // fix 14/6/2015 problem with change of dir access bits writing non-zero lengths to file
                   // because hdr structure is filled with length of what is in the folder on load
                   // but zip code does not keep this info updated
  {
   xsize=hdr->size;
   xlength=hdr->length;
  }
  else
  {
   xsize=xlength=0;
  }


  cp->version_made_by=VERSIONMADEBY;
  cp->version_needed_to_extract=VERSIONTOX;
  cp->general_purpose_bit_flag=opt;
  cp->compression_method=hdr->hdrver;
  cp->last_mod_file_time=time;
  cp->last_mod_file_date=date;
  cp->crc32=hdr->crc;
  cp->compressed_size=xsize;
  cp->uncompressed_size=xlength;
  cp->filename_length=strlen(name);
  cp->extra_field_length=exlen;
  cp->file_comment_length=0;
  cp->disk_number_start=0;
  cp->internal_file_attributes=0;


#define S_IFDIR  0040000
#define S_IFREG  0100000
#define S_IEXEC  0000100
#define S_IWRITE 0000200
#define S_IREAD  0000400

  attr=hdr->acc;
  st_mode=((attr & 0001)<<8) | ((attr & 0002)<<6)|
          ((attr & 0020)>>2) | ((attr & 0040)>>4);
  st_mode|=(hdr->dirn==DNFILE)?(S_IFREG):(S_IFDIR|0700);

  attr=(st_mode<<16);
  attr|=(hdr->dirn==DNFILE)?(0x0):(0x10);
  attr|=!(st_mode & S_IWRITE);

  cp->external_file_attributes=attr;

  cp->relative_offset_local_header=hdr->fp-4;

  memcpy(cp->extra,name,cp->filename_length);
  memcpy(cp->extra+cp->filename_length,extra,exlen);

  ep=(end_header *)(arc->end);

  ep->total_entries_central_dir_on_this_disk+=1;
  ep->total_entries_central_dir+=1;
  ep->size_central_directory+=rlen;
  ep->offset_start_central_directory+=chunk;

  arc->loc+=chunk;
 }
 return(err);
}




_kernel_oserror * create(linkblock * block)
{
 _kernel_oserror       * err;
 archive               * arc;
 int                     type;
 char                  * name;
 end_header  ehdr;

 err=NULL;

 arc=block->create.arc;
 name=arc->name;
 type=arc->type;

 err=openarc(arc,'w');

 if(!err)
 {
  initendheader(&ehdr);
  arc->size=0;
  if(!arc->cent) err=flex_alloc((flex_ptr)&(arc->cent),0);
  arc->loc=4;
  if(!arc->end) err=flex_alloc((flex_ptr)&(arc->end),sizeof(ehdr));
  if(!err)      memcpy((char*)arc->end,&ehdr,sizeof(ehdr));
  err=writeendheader(arc,&ehdr);
 }

 if(!err)
 {
  err=closearc(arc);
 }
 else
 {
  closearc(arc);
 }

 if(!err) err=settype(arc->name,*(((int*)archivetypes)+1));
 if(err)  delete(arc->name);

 return(err);
}



int  cxmethod=8;
int  cxcode=0;
char cxpassword[32];



_kernel_oserror * method(linkblock * block)
{
 _kernel_oserror * err;

 cxmethod=block->method.compressiontype & 0xFF;
 err=NULL;

 return(err);
}




_kernel_oserror * info(linkblock * block)
{
 _kernel_oserror * err;

 block->info.archivetypes=archivetypes;
 block->info.compressiontypes=compressiontypes;
 block->info.codetypes=codetypes;
 block->info.conversiontypes=conversiontypes;
 block->info.modulebase=(char*)Image_RO_Base;

 err=NULL;

 return(err);
}


_kernel_oserror * convert(linkblock * block)
{
 _kernel_oserror * err;

 err=NULL;
 block=block;

 return(err);
}


_kernel_oserror * zipusecentraldir(char * args[])
{
 int i;
 i=atoi(args[0]);

 usecentraldir=(i!=0);
 return(NULL);
}
